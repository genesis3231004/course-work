#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void del_by_value(struct Node** head, int num);
void deleteByKey(struct Node **head, int key);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);


int main()
{
    struct Node *head = NULL;
    int choice, data,data2;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by value\n");
        printf("5. Delete by Key\n");
        printf("6. Insert after Key\n");
        printf("7. Insert after Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        if(choice>8 && choice <0)
            printf("Option not available!");
        switch(choice){
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append:");
            scanf("%d",&data);
            append(&head,data);
            break;
        case 3:
            printf("Enter data to prepend:");
            scanf("%d",&data);
            prepend(&head,data);
            break;
        case 4:
            printf("Enter Value to delete from list:");
            scanf("%d",&data);
            del_by_value(&head,data);
            break;
        case 5:
            printf("Enter key:");
            scanf("%d",&data);
            deleteByKey(&head,data); 
            break;  
        case 6:
            printf("Enter key to insert after:");
            scanf("%d",&data);
            printf("Enter value to insert:");
            scanf("%d",&data2);
            insertAfterKey(&head,data,data2);
            break; 
        case 7:
            printf("Enter value to insert after:");
            scanf("%d",&data);
            printf("Enter value to insert:");
            scanf("%d",&data2);
            insertAfterValue(&head,data,data2);
            break;
        case 8:
            exit(1);    




        }
    }

    return 0;
}
struct Node *createNode(int num){
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
};

void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}


void del_by_value(struct Node **head, int num) {
    struct Node *current = *head;
    struct Node *previous = NULL;

    while (current != NULL) {
        if (current->number == num) {
            if (previous == NULL) {
                
                *head = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            break;
        }
        previous = current;
        current = current->next;
    }
}
void deleteByKey(struct Node **head, int key) {
    if (*head == NULL) {
        printf("List is empty!\n");
        return;
    }
    if (key == 0) {
        struct Node *current = *head;
        *head = (*head)->next;
        free(current);
        return;
    }
    int count = 0;
    struct Node *current = *head;
    struct Node *previous = NULL;
    while (current != NULL && count < key) {
        previous = current;
        current = current->next;
        count++;
    }
    if (current == NULL) {
        printf("Invalid index!\n");
        return;
    }
    if (previous != NULL) {
        previous->next = current->next;
    } else {
        *head = current->next;
    }
    free(current);
}
void insertAfterKey(struct Node **head, int key, int value) {
   
    int count = 0;
    struct Node *current = *head;
    struct Node *previous = NULL;
    while (current != NULL && count < key) {
        previous = current;
        current = current->next;
        count++;
    }
    
    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
}
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *current = *head;
    while (current != NULL && current->number != searchValue) {
        current = current->next;
    }

    if (current == NULL) {
        printf("Value unavailable!\n");
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next=newNode;
}
